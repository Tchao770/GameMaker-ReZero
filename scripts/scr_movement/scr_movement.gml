scr_getinput();


var move = key_D - key_A;
hsp = move * walksp;
vsp += grv;

// Horizontal Collision
if(place_meeting(x + hsp, y, obj_wall)){
	while(!place_meeting(x + sign(hsp), y, obj_wall)){
		x += sign(hsp);
	}hsp = 0;
}x += hsp;


// Vertical Collision
if(place_meeting(x, y + vsp, obj_wall)){
	while(!place_meeting(x, y + sign(vsp), obj_wall)){
		y += sign(vsp);
	}vsp = 0;
}y += vsp;

// Jump function
if(place_meeting(x, y + 1, obj_wall)) && (jump){
	vsp = -8;
}


///////////////////////////////////////////////////////////////////////////


// **Sprite Animations** //
 if(!place_meeting(x, y + 1, obj_wall)){
	sprite_index = s_player_jump_temp;
	image_speed = 0;
	if(sign(vsp) > 0) image_index = 1; 
	else image_index = 0;
 }
 else{
	image_speed = 1;
	if(hsp == 0) sprite_index = s_player_idle; else sprite_index = s_player_run;
 }
 
 // Mirrors sprite animation
 if(hsp != 0)image_xscale = sign(hsp) * 2;
 
 // Dash animation
 if(dash){
	sprite_index = s_player_dash;
	image_speed = 1;
	if(sign(vsp) > 0){
		image_speed = 0;
		image_index = 2;
	}
	//instance_create_layer(x, y, "L_effects", obj_dash_effect);
	hsp = move * dashsp;
	x += hsp;
	grv = 0.3;
 }else grv = 0.3;
 
 if(action){
	state = states.attacks;
 }

 