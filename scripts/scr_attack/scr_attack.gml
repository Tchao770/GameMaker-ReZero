scr_getinput();


// Attack animation
if(action){
	sprite_index = s_player_attack1;
	image_speed = 1;
	x += hsp;
	vsp += grv;
	//y += vsp;
	if(image_index >= 1) && (image_index <= 3){
		with(instance_create_layer(x, y, "L_projectile", obj_attack1_hitbox)){
			image_xscale = other.image_xscale;
			with(instance_place(x, y, obj_enemy)){
				if(hp == 0){
					hp = 1;
					vsp = -3;
					hsp = sign(x - other.x) * 4;
					image_xscale = sign(hsp);
				}
			}
		}
	}
}
else state = states.normal;
/*
if(action) && (canAttack){
	canAttack = false;
	image_speed = 2;
	sprite_index = sprite[combo];
	combo = (combo + 1) % comboMax;
	alarm[0] = 10;
	alarm[1] = 15;
}
else state = states.normal;
*/