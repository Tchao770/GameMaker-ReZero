{
    "id": "d0731b32-c455-47bb-88a1-84e97c65a12b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy",
    "eventList": [
        {
            "id": "98513a72-268d-4397-bebc-aede2ab34d75",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d0731b32-c455-47bb-88a1-84e97c65a12b"
        },
        {
            "id": "ff99bd8c-b07c-4b96-addf-f5f7811022a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d0731b32-c455-47bb-88a1-84e97c65a12b"
        },
        {
            "id": "67844ad1-6ee9-4147-8e06-4e4c2cbb0480",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "419092f6-80d4-4c0c-a883-18982ae515d6",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d0731b32-c455-47bb-88a1-84e97c65a12b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "42e988fa-58bf-4762-85f8-83e33064ba7a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "53971a7f-b44a-4aef-9b12-62f0c68322c1",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "385ce30d-e126-4a39-ba92-bbdf6b5e060a",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 357,
            "y": 0
        },
        {
            "id": "79c99195-f84b-4341-a3b8-503cfad43c3c",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 357,
            "y": 340
        },
        {
            "id": "178d7b1f-6f71-4a56-8868-2bc6a2197243",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 340
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8ee17ebe-a3b4-4b5e-8fb0-3442a3b91d59",
    "visible": true
}