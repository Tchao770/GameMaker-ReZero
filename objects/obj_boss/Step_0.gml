if(instance_exists(obj_player)){
	move_towards_point(obj_player.x,obj_player.y,spd)
	//move_towards_point(x,y,spd)
}
//image_angle = direction;
if(hp <= 0) {
	with(obj_text) score += 5;
	instance_destroy();
}
// Horizontal Collision
if(place_meeting(x + hsp, y, obj_wall)){
	while(!place_meeting(x + sign(hsp), y, obj_wall)){
		x += sign(hsp);
	}hsp = 0;
}x += hsp;


// Vertical Collision
if(place_meeting(x, y + vsp, obj_wall)){
	while(!place_meeting(x, y + sign(vsp), obj_wall)){
		y += sign(vsp);
	}vsp = 0;
}y += vsp;