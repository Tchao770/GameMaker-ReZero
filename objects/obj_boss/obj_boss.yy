{
    "id": "e7daecc6-ede7-475c-9d58-67f1dce150e8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_boss",
    "eventList": [
        {
            "id": "8bc25f73-f678-4eea-a38e-b5afc5955701",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e7daecc6-ede7-475c-9d58-67f1dce150e8"
        },
        {
            "id": "222d4d61-d1d2-427c-ac9b-245bbcdf1d61",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e7daecc6-ede7-475c-9d58-67f1dce150e8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "97fe7fe9-ce48-4c27-867d-4cf07c052d76",
    "visible": true
}