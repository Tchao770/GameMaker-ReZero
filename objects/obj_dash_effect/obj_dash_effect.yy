{
    "id": "ab3e52e0-2e41-478d-b9ca-819fc4a5d83b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_dash_effect",
    "eventList": [
        {
            "id": "1b0294cd-dfc6-4bcc-8629-48de9cb2aa2f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ab3e52e0-2e41-478d-b9ca-819fc4a5d83b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d0731b32-c455-47bb-88a1-84e97c65a12b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ee708407-ed6f-4c22-8ad2-037e7b9056ee",
    "visible": true
}