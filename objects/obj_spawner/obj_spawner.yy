{
    "id": "12988a7d-68ca-4ae7-b2ae-e594fe2c4305",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_spawner",
    "eventList": [
        {
            "id": "03999ca6-0172-40e8-ad08-1285edef2b27",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "12988a7d-68ca-4ae7-b2ae-e594fe2c4305"
        },
        {
            "id": "8903fe87-c3a0-4a12-b2bc-ab1f8b066c66",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "12988a7d-68ca-4ae7-b2ae-e594fe2c4305"
        },
        {
            "id": "a9ccc2f5-d2f8-4733-9a4f-d4e124c50d4c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "12988a7d-68ca-4ae7-b2ae-e594fe2c4305"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}