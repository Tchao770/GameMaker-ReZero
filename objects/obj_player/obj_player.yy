{
    "id": "419092f6-80d4-4c0c-a883-18982ae515d6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "1f3a72b6-a778-4b75-862e-804a2b52be87",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "419092f6-80d4-4c0c-a883-18982ae515d6"
        },
        {
            "id": "14c62cc3-a631-41bc-ac8e-b2f0a24748ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "419092f6-80d4-4c0c-a883-18982ae515d6"
        },
        {
            "id": "c38c80f6-6f9d-420d-8dd0-dd43e879ae96",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "419092f6-80d4-4c0c-a883-18982ae515d6"
        },
        {
            "id": "278486d7-56c4-4ed6-986d-5f40095c08bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "419092f6-80d4-4c0c-a883-18982ae515d6"
        }
    ],
    "maskSpriteId": "44ee1537-3a3b-4531-8805-2a4cf9390846",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "44ee1537-3a3b-4531-8805-2a4cf9390846",
    "visible": true
}