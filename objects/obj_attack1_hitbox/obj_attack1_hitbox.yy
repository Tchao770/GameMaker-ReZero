{
    "id": "177fd2e8-8218-425b-a546-bb2f1dccc3d7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_attack1_hitbox",
    "eventList": [
        {
            "id": "c1299847-d953-44e0-9dbd-586f8b237508",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 77,
            "eventtype": 8,
            "m_owner": "177fd2e8-8218-425b-a546-bb2f1dccc3d7"
        },
        {
            "id": "8fbdfc70-1183-4525-b751-9ae556a81a16",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d0731b32-c455-47bb-88a1-84e97c65a12b",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "177fd2e8-8218-425b-a546-bb2f1dccc3d7"
        },
        {
            "id": "06f9d851-6e78-48d0-bbb8-8e7d2c903261",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "e7daecc6-ede7-475c-9d58-67f1dce150e8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "177fd2e8-8218-425b-a546-bb2f1dccc3d7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "83e7420e-ec4c-4e42-b101-665e33b68bd8",
    "visible": false
}