{
    "id": "c12ee2a2-7960-4502-a2ff-5f67157cc49f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_espawn",
    "eventList": [
        {
            "id": "624a6f6a-10f2-465a-ba3b-042bc962a5bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c12ee2a2-7960-4502-a2ff-5f67157cc49f"
        },
        {
            "id": "13e688c4-a336-4c93-b6c0-9b1054ed331c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c12ee2a2-7960-4502-a2ff-5f67157cc49f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "97fe7fe9-ce48-4c27-867d-4cf07c052d76",
    "visible": true
}