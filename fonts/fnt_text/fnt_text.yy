{
    "id": "5a980020-a78c-4835-ac2c-479144e7521b",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_text",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "MS Gothic",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "02efe2d2-b357-438b-9268-c1870f698a67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 48,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "3ee06c3b-d273-4c3e-bd5f-f91b4ebe95be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 48,
                "offset": 9,
                "shift": 24,
                "w": 6,
                "x": 397,
                "y": 152
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "0231c813-bc7c-4551-817e-2a2aa3967e27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 180,
                "y": 102
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "52ca7d2e-2d39-426b-b475-456dcbcdd1af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 74,
                "y": 52
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "4d34f58c-da8b-40fa-ac2e-acc690469f38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 19,
                "x": 89,
                "y": 152
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "4435b14a-74ac-4032-b30e-959a753055e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 26,
                "y": 52
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "53615d18-de60-4bc9-9704-8b0603bff961",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 170,
                "y": 52
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "151ae074-9257-438b-a380-149918a0f8d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 48,
                "offset": 4,
                "shift": 24,
                "w": 5,
                "x": 426,
                "y": 152
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "4d59025e-65e8-407f-ac89-5189d736413f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 48,
                "offset": 11,
                "shift": 24,
                "w": 12,
                "x": 325,
                "y": 152
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "96fcb8cc-f520-447f-a91c-c6849932c520",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 12,
                "x": 297,
                "y": 152
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "0dbcd506-16ce-4bfb-84ca-7cac81d2aecb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 48,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 132,
                "y": 2
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "6c032351-e456-4c20-a817-b75428a712ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 98,
                "y": 52
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "91399458-abbd-4feb-9897-9d720cf5c711",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 48,
                "offset": 4,
                "shift": 24,
                "w": 5,
                "x": 405,
                "y": 152
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "a03c2b62-0991-4ab7-8bea-c79ad8216d84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 122,
                "y": 52
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "d3d6b6bf-d4e5-4cb5-868a-711b80647191",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 48,
                "offset": 3,
                "shift": 24,
                "w": 6,
                "x": 389,
                "y": 152
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "a09bf7cd-5423-4e43-bf2e-b9121618090f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 48,
                "offset": 0,
                "shift": 24,
                "w": 23,
                "x": 284,
                "y": 2
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "c980a753-42a2-4798-acab-9d13b8270740",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 146,
                "y": 52
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "8683c239-c1e2-4cfc-9435-4d6a0e25b43b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 48,
                "offset": 4,
                "shift": 24,
                "w": 11,
                "x": 352,
                "y": 152
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "0c992739-545a-43c8-b99f-73262d8586bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 312,
                "y": 102
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "98ca21be-3e58-45c8-8f63-d8e6fbd8ab67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 334,
                "y": 102
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "cc07c530-440a-44f1-a7a0-016595c75f72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 50,
                "y": 52
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "039c2902-7420-40b5-8599-31c242300042",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 268,
                "y": 102
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "73b917de-0d02-467b-a2e0-459e972fe6c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 24,
                "y": 152
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "39eb31eb-4135-4175-a927-4c0a24c71a41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 48,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 215,
                "y": 152
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "152ed733-a026-4920-94a3-fe0b15ca76e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 2,
                "y": 152
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "4906a4ac-8a74-4cfc-a5cf-c8056cf1f2f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 488,
                "y": 102
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "5f0df55e-57d4-4b57-b65d-36685073d74a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 48,
                "offset": 9,
                "shift": 24,
                "w": 5,
                "x": 433,
                "y": 152
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "c014ebc5-1d02-424e-b83a-69cc4e265de2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 48,
                "offset": 9,
                "shift": 24,
                "w": 5,
                "x": 412,
                "y": 152
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "75aa073a-5efe-4fa9-a45c-463019cf9c7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 477,
                "y": 2
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "321cf5e7-d8df-423a-9c6c-78e492d99cb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 429,
                "y": 2
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "6f557268-2240-4043-8dc7-6ca00f783376",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 309,
                "y": 2
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "75d0bab2-4e11-46ba-abba-0e67ebebc4ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 19,
                "x": 194,
                "y": 152
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "de00d8db-f6e9-407d-94aa-2bb893330965",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 333,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "7018b04d-425d-4aec-8850-8dc33209c31e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 48,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 158,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "9fe35a07-2ed0-4dda-8e6b-f0b89bb93226",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 21,
                "x": 470,
                "y": 52
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "029ead5f-99b0-4817-a9ad-5d4de243f251",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 453,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "b27c8e7c-578b-4d79-94a8-31696088ad88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 21,
                "x": 401,
                "y": 52
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "64275cc2-12ec-4fab-8311-00f4f5a54cee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 356,
                "y": 102
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "e77d53c5-264e-44a3-b0c5-9073e5baa775",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 21,
                "x": 378,
                "y": 52
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "e50955b3-35ef-49ba-8a3f-974884b4d562",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 355,
                "y": 52
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "c860dce3-825b-4bf7-9c8b-33070f9e9683",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 378,
                "y": 102
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "a3c6b189-b0cc-420a-9999-548fc87340f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 48,
                "offset": 7,
                "shift": 24,
                "w": 10,
                "x": 365,
                "y": 152
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "27e6019e-1587-436d-b0ba-263d37e83157",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 400,
                "y": 102
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "15a7a3c9-73af-49af-8730-49e0748528bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 22,
                "x": 381,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "474427c6-3d73-4cf9-b66f-501c9d7d852b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 422,
                "y": 102
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "b6d917d0-8813-46cf-ba80-0cdc1af6750d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 405,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "5d78d799-c90a-4116-8e7f-843679914a13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 466,
                "y": 102
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "6e342f98-d303-4a75-be86-7c8631dc9238",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 2,
                "y": 52
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "7dc84fbc-1a29-4515-9456-4e17f49e13e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 21,
                "x": 263,
                "y": 52
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "600c691d-0a17-4d14-880b-0226b6e161e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 357,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "4f75d9fc-6493-4638-b865-2bb7d102e97e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 21,
                "x": 217,
                "y": 52
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "001e1906-1b8d-4bf6-9e90-c1ec8f7571ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 424,
                "y": 52
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "fbc06253-6143-4863-bb91-da9d14b45a6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 46,
                "y": 152
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "428484a6-917d-448b-8ef8-8069e6f80f90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 444,
                "y": 102
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "5d2d5fe3-9ac1-4ab2-9dae-17fa199cd353",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 48,
                "offset": 0,
                "shift": 24,
                "w": 23,
                "x": 259,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "a9710c12-bad8-45a5-bd3d-15fe878ad0df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 48,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "eee82884-7850-4b60-9b6b-24bae720230a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 48,
                "offset": 0,
                "shift": 24,
                "w": 23,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "9f9a9d23-6183-4078-9cfc-24a4d0736e23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 48,
                "offset": 0,
                "shift": 24,
                "w": 23,
                "x": 209,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "ca1ff206-e91e-46bc-8264-a3f75606c9f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 286,
                "y": 52
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "560b9aa1-4928-420d-8692-6a7cf9f88740",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 48,
                "offset": 9,
                "shift": 24,
                "w": 13,
                "x": 282,
                "y": 152
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "ecb06bf4-2351-4f8e-9b20-f56562c34e82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 48,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 80,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "7cd75745-068a-4f30-a49e-07c1320099a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 13,
                "x": 267,
                "y": 152
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "76bf7df2-0994-4aaa-879d-941873e1ab1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 19,
                "x": 173,
                "y": 152
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "25278092-d96a-4759-bc6f-a03e63366a76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 48,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "1c18d066-9dc2-4b62-ab2a-c0187a605936",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 48,
                "offset": 6,
                "shift": 24,
                "w": 12,
                "x": 311,
                "y": 152
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "56ba1557-be5a-4df4-bd47-2151de372d4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 136,
                "y": 102
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "6f05aa39-4089-4ef8-9c2e-1f7ac3107f36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 114,
                "y": 102
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "ef83505d-a499-4799-b485-b1f6ecf6930a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 92,
                "y": 102
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "d9525671-b4a6-432a-b4bb-657d079dfa6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 20,
                "x": 70,
                "y": 102
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "acd479be-4e8b-4a4d-99f9-7b2329c3b80b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 246,
                "y": 102
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "a62ac777-cdfc-45c1-a81f-0bccbd781972",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 224,
                "y": 102
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "326a062b-2fed-442c-97e0-e79f74c473b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 21,
                "x": 2,
                "y": 102
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "8b677570-1dfa-4753-b14b-b16e8c358063",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 19,
                "x": 131,
                "y": 152
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "eb5229c2-b976-4035-97b5-7f3529c85fbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 48,
                "offset": 9,
                "shift": 24,
                "w": 5,
                "x": 419,
                "y": 152
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "a11e2050-8bc5-444c-8adf-6c8c51469775",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 48,
                "offset": 4,
                "shift": 24,
                "w": 14,
                "x": 251,
                "y": 152
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "1405be96-8818-4f34-a3af-77917c0b2d35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 48,
                "offset": 3,
                "shift": 24,
                "w": 20,
                "x": 158,
                "y": 102
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "c3ac994d-c0da-4f0d-8d35-501865bc8d64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 48,
                "offset": 10,
                "shift": 24,
                "w": 4,
                "x": 440,
                "y": 152
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "40fad023-d9db-4be9-879a-c103359ef84c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 48,
                "offset": 0,
                "shift": 24,
                "w": 23,
                "x": 184,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "65b878f0-de07-4ec6-952a-788afa6e02cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 202,
                "y": 102
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "6a137414-cfd0-4dca-aa2f-f6bc3c116f0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 194,
                "y": 52
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "2e6914ba-78e7-4129-b487-13e0f23883e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 48,
                "y": 102
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "93b675bc-5b72-4efb-b216-9aed7c6aeecc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 25,
                "y": 102
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "b2f9127d-f7cf-4b50-9e2b-4c91a9735ab0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 48,
                "offset": 5,
                "shift": 24,
                "w": 14,
                "x": 235,
                "y": 152
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "7001e2f1-686c-4b6b-ac67-16a80f19f62d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 19,
                "x": 110,
                "y": 152
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "649b013f-1c45-47a4-8f98-0d6cfd769f48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 447,
                "y": 52
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "4614c611-82ee-4414-9d2a-f4480fd0d2b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 19,
                "x": 68,
                "y": 152
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "ba6d71d0-f65a-4ae9-81c4-d2ed00febf19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 332,
                "y": 52
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "6d8728bc-0c9d-4968-992d-786f9acf8e25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 48,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 28,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "400a10d6-be2e-4109-98bc-3e07d757ee27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 309,
                "y": 52
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "5974286b-7ffb-4f89-9389-03487272bc3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 240,
                "y": 52
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "b8125b65-3b51-4cd0-9657-8871df52bae0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 19,
                "x": 152,
                "y": 152
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "da290885-b362-4650-8511-749d56f98966",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 48,
                "offset": 10,
                "shift": 24,
                "w": 11,
                "x": 339,
                "y": 152
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "afaeb3d2-c053-41e4-adc4-06ccfebfc236",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 48,
                "offset": 10,
                "shift": 24,
                "w": 4,
                "x": 446,
                "y": 152
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "eddc92c3-3f4e-4b4d-b1f2-bcb3c0d010a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 48,
                "offset": 3,
                "shift": 24,
                "w": 10,
                "x": 377,
                "y": 152
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "266e07ed-5ab8-4121-a373-f49389039026",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 290,
                "y": 102
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 36,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}