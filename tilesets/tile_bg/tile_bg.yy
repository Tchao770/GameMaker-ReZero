{
    "id": "11533bd9-83b6-4f60-b955-9e0543a52a96",
    "modelName": "GMTileSet",
    "mvc": "1.11",
    "name": "tile_bg",
    "auto_tile_sets": [
        
    ],
    "macroPageTiles": {
        "SerialiseData": null,
        "SerialiseHeight": 0,
        "SerialiseWidth": 0,
        "TileSerialiseData": [
            
        ]
    },
    "out_columns": 3,
    "out_tilehborder": 2,
    "out_tilevborder": 2,
    "spriteId": "98c39d3a-9da3-4f92-96f6-7b3e0ead6a01",
    "sprite_no_export": true,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "tile_animation": {
        "AnimationCreationOrder": null,
        "FrameData": [
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11
        ],
        "SerialiseFrameCount": 1
    },
    "tile_animation_frames": [
        
    ],
    "tile_animation_speed": 15,
    "tile_count": 12,
    "tileheight": 128,
    "tilehsep": 0,
    "tilevsep": 0,
    "tilewidth": 128,
    "tilexoff": 0,
    "tileyoff": 0
}