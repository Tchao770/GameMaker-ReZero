{
    "id": "ee708407-ed6f-4c22-8ad2-037e7b9056ee",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_dash_particle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 0,
    "bbox_right": 20,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2e196f9a-a25c-469a-a21c-3e7e417ddac4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee708407-ed6f-4c22-8ad2-037e7b9056ee",
            "compositeImage": {
                "id": "56204ed8-a4b2-46df-afad-e0c248b34b54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e196f9a-a25c-469a-a21c-3e7e417ddac4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b655233-3018-45a9-acb5-5e2051a328fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e196f9a-a25c-469a-a21c-3e7e417ddac4",
                    "LayerId": "70428e12-45fe-4988-b093-24228cdfcaf2"
                }
            ]
        },
        {
            "id": "76b610bc-8955-4184-8d2c-3b0c566d45b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee708407-ed6f-4c22-8ad2-037e7b9056ee",
            "compositeImage": {
                "id": "8ff2d5a8-848a-4e49-bf98-e892f81b5e64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76b610bc-8955-4184-8d2c-3b0c566d45b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bad6f518-542a-45ae-86f3-c12af7b74f5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76b610bc-8955-4184-8d2c-3b0c566d45b1",
                    "LayerId": "70428e12-45fe-4988-b093-24228cdfcaf2"
                }
            ]
        },
        {
            "id": "4fb738f6-adaa-41ff-ab93-24ec233f2e8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee708407-ed6f-4c22-8ad2-037e7b9056ee",
            "compositeImage": {
                "id": "19918a74-b770-4bd6-9984-b3fcf966a0eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fb738f6-adaa-41ff-ab93-24ec233f2e8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "819f3ad5-da57-482c-a07e-0a696e696a7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fb738f6-adaa-41ff-ab93-24ec233f2e8b",
                    "LayerId": "70428e12-45fe-4988-b093-24228cdfcaf2"
                }
            ]
        },
        {
            "id": "0e09de16-d1d4-4085-b386-a8c12382615d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee708407-ed6f-4c22-8ad2-037e7b9056ee",
            "compositeImage": {
                "id": "8c6cb92c-0108-48ce-9607-b4f89735012e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e09de16-d1d4-4085-b386-a8c12382615d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66db596a-8c9c-4fe3-a277-4d71acbf0a0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e09de16-d1d4-4085-b386-a8c12382615d",
                    "LayerId": "70428e12-45fe-4988-b093-24228cdfcaf2"
                }
            ]
        },
        {
            "id": "cb11fa62-98cd-4328-82c9-b1dd018fd2fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee708407-ed6f-4c22-8ad2-037e7b9056ee",
            "compositeImage": {
                "id": "614c670c-7b57-4370-9290-351a846e3b8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb11fa62-98cd-4328-82c9-b1dd018fd2fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60d1a98a-87cf-49fb-abdd-e572b7958c23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb11fa62-98cd-4328-82c9-b1dd018fd2fb",
                    "LayerId": "70428e12-45fe-4988-b093-24228cdfcaf2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 57,
    "layers": [
        {
            "id": "70428e12-45fe-4988-b093-24228cdfcaf2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ee708407-ed6f-4c22-8ad2-037e7b9056ee",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 21,
    "xorig": 10,
    "yorig": 28
}