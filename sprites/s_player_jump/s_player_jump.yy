{
    "id": "0f81f332-d9ca-4725-96b4-f75483680cef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_jump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 0,
    "bbox_right": 37,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2bc1ee28-ed01-464e-95da-6502f8a64f6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f81f332-d9ca-4725-96b4-f75483680cef",
            "compositeImage": {
                "id": "418f277e-30cd-46ae-be6b-ff8ec61c1f88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bc1ee28-ed01-464e-95da-6502f8a64f6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f972c58-17a3-4150-9d9f-ae987a6754ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bc1ee28-ed01-464e-95da-6502f8a64f6c",
                    "LayerId": "192620e0-3742-478c-868d-4a81c99a2e7a"
                }
            ]
        },
        {
            "id": "d6135787-1dab-4bf9-999c-f0d292b5d58b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f81f332-d9ca-4725-96b4-f75483680cef",
            "compositeImage": {
                "id": "8b30fef3-460e-45be-9b3f-7b87d40ff81a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6135787-1dab-4bf9-999c-f0d292b5d58b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3689a4b-7286-4e0b-83ce-707361175261",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6135787-1dab-4bf9-999c-f0d292b5d58b",
                    "LayerId": "192620e0-3742-478c-868d-4a81c99a2e7a"
                }
            ]
        },
        {
            "id": "01129fb3-151c-45bc-a85b-145e0b83b660",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f81f332-d9ca-4725-96b4-f75483680cef",
            "compositeImage": {
                "id": "a10eb941-aeb1-4648-bbcd-0d4dd7fa0b69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01129fb3-151c-45bc-a85b-145e0b83b660",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2756500-6a6e-4af3-a41a-6be879366819",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01129fb3-151c-45bc-a85b-145e0b83b660",
                    "LayerId": "192620e0-3742-478c-868d-4a81c99a2e7a"
                }
            ]
        },
        {
            "id": "8f40ae60-1846-4766-8ce6-00d465b0868e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f81f332-d9ca-4725-96b4-f75483680cef",
            "compositeImage": {
                "id": "9fc6d7c5-f646-42d3-9066-2a619b844b27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f40ae60-1846-4766-8ce6-00d465b0868e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b194eb12-d20e-4959-975d-e9a3b4a427ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f40ae60-1846-4766-8ce6-00d465b0868e",
                    "LayerId": "192620e0-3742-478c-868d-4a81c99a2e7a"
                }
            ]
        },
        {
            "id": "5b7d2603-6293-4dcf-abd4-c96aaf8eb0eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f81f332-d9ca-4725-96b4-f75483680cef",
            "compositeImage": {
                "id": "fd978e00-d9c1-4709-b0cc-0f9d7d299040",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b7d2603-6293-4dcf-abd4-c96aaf8eb0eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d9d2fb2-6065-462c-ad3f-a9cf64d8f351",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b7d2603-6293-4dcf-abd4-c96aaf8eb0eb",
                    "LayerId": "192620e0-3742-478c-868d-4a81c99a2e7a"
                }
            ]
        },
        {
            "id": "4caea754-fc58-4565-8fc7-8e6faf8ad27e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f81f332-d9ca-4725-96b4-f75483680cef",
            "compositeImage": {
                "id": "44c2e6ec-c699-482b-9fc4-7b5d356d79ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4caea754-fc58-4565-8fc7-8e6faf8ad27e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1886efa-bcc0-4790-9fe0-be3dcd4bb4b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4caea754-fc58-4565-8fc7-8e6faf8ad27e",
                    "LayerId": "192620e0-3742-478c-868d-4a81c99a2e7a"
                }
            ]
        },
        {
            "id": "b29b8082-c2da-483a-8a98-eed1155bb2a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f81f332-d9ca-4725-96b4-f75483680cef",
            "compositeImage": {
                "id": "2b7279bc-7156-4122-819f-58164ce20052",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b29b8082-c2da-483a-8a98-eed1155bb2a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0eb3754-3064-42ba-876b-e70827411bc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b29b8082-c2da-483a-8a98-eed1155bb2a5",
                    "LayerId": "192620e0-3742-478c-868d-4a81c99a2e7a"
                }
            ]
        },
        {
            "id": "005edf3c-2063-4ceb-8d47-541a556527cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f81f332-d9ca-4725-96b4-f75483680cef",
            "compositeImage": {
                "id": "81439ca0-c265-47ba-890c-e7db4b5add87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "005edf3c-2063-4ceb-8d47-541a556527cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "baafae65-80d7-4e0c-9dcc-8a4dbe44b68d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "005edf3c-2063-4ceb-8d47-541a556527cf",
                    "LayerId": "192620e0-3742-478c-868d-4a81c99a2e7a"
                }
            ]
        },
        {
            "id": "eadae330-44c8-4af9-9754-1944bb4ef8cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f81f332-d9ca-4725-96b4-f75483680cef",
            "compositeImage": {
                "id": "44f9bcd9-ed04-499f-a1bb-5c4c44948407",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eadae330-44c8-4af9-9754-1944bb4ef8cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ea7a5a0-1e51-4c2a-8ac7-7a2b4474b193",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eadae330-44c8-4af9-9754-1944bb4ef8cb",
                    "LayerId": "192620e0-3742-478c-868d-4a81c99a2e7a"
                }
            ]
        },
        {
            "id": "3e7e95ac-63d1-40a3-bbb4-f63f8c0e1532",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f81f332-d9ca-4725-96b4-f75483680cef",
            "compositeImage": {
                "id": "89f5914c-db33-444b-8682-3835537b5990",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e7e95ac-63d1-40a3-bbb4-f63f8c0e1532",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13a0099c-adaa-4209-ac6c-8e1bb3016ce0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e7e95ac-63d1-40a3-bbb4-f63f8c0e1532",
                    "LayerId": "192620e0-3742-478c-868d-4a81c99a2e7a"
                }
            ]
        },
        {
            "id": "d0be9c81-98d0-4498-87fa-3a5b57dbba16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f81f332-d9ca-4725-96b4-f75483680cef",
            "compositeImage": {
                "id": "8c592ffd-24fe-4f05-8baa-d33525738760",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0be9c81-98d0-4498-87fa-3a5b57dbba16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5894444a-d219-4716-85b2-b9b84f3e2660",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0be9c81-98d0-4498-87fa-3a5b57dbba16",
                    "LayerId": "192620e0-3742-478c-868d-4a81c99a2e7a"
                }
            ]
        },
        {
            "id": "819f71aa-da83-4e9e-abe2-638f6459c19d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f81f332-d9ca-4725-96b4-f75483680cef",
            "compositeImage": {
                "id": "ac1f970e-fc66-443a-93e5-b0abce845087",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "819f71aa-da83-4e9e-abe2-638f6459c19d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d315bf8f-0389-495d-8249-0ba265a76815",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "819f71aa-da83-4e9e-abe2-638f6459c19d",
                    "LayerId": "192620e0-3742-478c-868d-4a81c99a2e7a"
                }
            ]
        },
        {
            "id": "dfb1675a-142c-4ace-a445-87c09437191c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f81f332-d9ca-4725-96b4-f75483680cef",
            "compositeImage": {
                "id": "044e1b5f-b945-410f-99b0-41a87cf5ab47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfb1675a-142c-4ace-a445-87c09437191c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5a0e7be-da55-49ef-96e5-2340ed573c60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfb1675a-142c-4ace-a445-87c09437191c",
                    "LayerId": "192620e0-3742-478c-868d-4a81c99a2e7a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "192620e0-3742-478c-868d-4a81c99a2e7a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f81f332-d9ca-4725-96b4-f75483680cef",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 38,
    "xorig": 19,
    "yorig": 24
}