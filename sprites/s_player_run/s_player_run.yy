{
    "id": "df989897-37ab-419a-ac19-6d14ca072e0a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 36,
    "bbox_left": 13,
    "bbox_right": 25,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8cbf70cb-7519-4e13-9a15-0eefb94b11b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df989897-37ab-419a-ac19-6d14ca072e0a",
            "compositeImage": {
                "id": "c3bb9573-1c5f-4c7d-804a-ce920e947857",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cbf70cb-7519-4e13-9a15-0eefb94b11b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cf6dbce-7679-469a-ae2e-2865172ce255",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cbf70cb-7519-4e13-9a15-0eefb94b11b7",
                    "LayerId": "80fbf8aa-dc63-4c98-867a-005dae4b1e0c"
                }
            ]
        },
        {
            "id": "5d06006b-7ad3-46f9-9c10-4bc7b4a8b817",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df989897-37ab-419a-ac19-6d14ca072e0a",
            "compositeImage": {
                "id": "17682086-ffcc-402a-b096-12b2a4550ef1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d06006b-7ad3-46f9-9c10-4bc7b4a8b817",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb0c82e4-2c5a-4d62-bf5e-1ac61053cac0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d06006b-7ad3-46f9-9c10-4bc7b4a8b817",
                    "LayerId": "80fbf8aa-dc63-4c98-867a-005dae4b1e0c"
                }
            ]
        },
        {
            "id": "a3a49830-4773-462a-8b75-ea1de6f0a378",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df989897-37ab-419a-ac19-6d14ca072e0a",
            "compositeImage": {
                "id": "c9170fb7-bee7-41ef-bf80-ea61b987c268",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3a49830-4773-462a-8b75-ea1de6f0a378",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2db8c7c4-1796-4e98-be16-e57b64a0436b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3a49830-4773-462a-8b75-ea1de6f0a378",
                    "LayerId": "80fbf8aa-dc63-4c98-867a-005dae4b1e0c"
                }
            ]
        },
        {
            "id": "2a56db60-d118-493f-8cf6-74039cf31c7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df989897-37ab-419a-ac19-6d14ca072e0a",
            "compositeImage": {
                "id": "a6d2a7ec-8d95-4dba-accb-c6694e34625b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a56db60-d118-493f-8cf6-74039cf31c7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d55e08b0-71f2-4f02-85ac-775c69621d69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a56db60-d118-493f-8cf6-74039cf31c7b",
                    "LayerId": "80fbf8aa-dc63-4c98-867a-005dae4b1e0c"
                }
            ]
        },
        {
            "id": "3db0ed60-f22b-4c73-bb1e-4826e3c5783a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df989897-37ab-419a-ac19-6d14ca072e0a",
            "compositeImage": {
                "id": "27ced9af-935a-45ee-8c51-0e89be019319",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3db0ed60-f22b-4c73-bb1e-4826e3c5783a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccafb443-ba33-46ff-960b-f7b5816dbbfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3db0ed60-f22b-4c73-bb1e-4826e3c5783a",
                    "LayerId": "80fbf8aa-dc63-4c98-867a-005dae4b1e0c"
                }
            ]
        },
        {
            "id": "8083480c-e919-4cd9-9fd3-fcf51b8e0af8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df989897-37ab-419a-ac19-6d14ca072e0a",
            "compositeImage": {
                "id": "7a2cb45b-0991-4672-b908-bc3aa9ede370",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8083480c-e919-4cd9-9fd3-fcf51b8e0af8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34eaf6ae-b7eb-4286-abee-98b11348ec2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8083480c-e919-4cd9-9fd3-fcf51b8e0af8",
                    "LayerId": "80fbf8aa-dc63-4c98-867a-005dae4b1e0c"
                }
            ]
        },
        {
            "id": "1ddd2fe0-e3bd-4588-ad2d-fbbb97493204",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df989897-37ab-419a-ac19-6d14ca072e0a",
            "compositeImage": {
                "id": "c0fc2de7-02af-4263-a29b-76d9941d7797",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ddd2fe0-e3bd-4588-ad2d-fbbb97493204",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "772f2bc0-1508-42bf-a3a3-5a46f0ae16da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ddd2fe0-e3bd-4588-ad2d-fbbb97493204",
                    "LayerId": "80fbf8aa-dc63-4c98-867a-005dae4b1e0c"
                }
            ]
        },
        {
            "id": "74e8349f-d59b-457f-9af4-8dc6d0dbe838",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df989897-37ab-419a-ac19-6d14ca072e0a",
            "compositeImage": {
                "id": "cb6ba5df-2034-4d96-9842-575cc94cf861",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74e8349f-d59b-457f-9af4-8dc6d0dbe838",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfbdc62a-8e2b-42f3-a1c8-0328d2dfa49c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74e8349f-d59b-457f-9af4-8dc6d0dbe838",
                    "LayerId": "80fbf8aa-dc63-4c98-867a-005dae4b1e0c"
                }
            ]
        },
        {
            "id": "e5c1fdf0-e633-49b4-89e6-afdc65f99ee2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df989897-37ab-419a-ac19-6d14ca072e0a",
            "compositeImage": {
                "id": "d6dfa1ca-8ca6-4cbf-8533-edbe9e8202c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5c1fdf0-e633-49b4-89e6-afdc65f99ee2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c9bcf5f-6c40-4987-aa7c-2cd1ca2cf448",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5c1fdf0-e633-49b4-89e6-afdc65f99ee2",
                    "LayerId": "80fbf8aa-dc63-4c98-867a-005dae4b1e0c"
                }
            ]
        },
        {
            "id": "45694eaa-188f-4725-83b5-401740c99ae0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df989897-37ab-419a-ac19-6d14ca072e0a",
            "compositeImage": {
                "id": "23ee8ea5-706e-4998-9578-17f65765c2f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45694eaa-188f-4725-83b5-401740c99ae0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02ad8930-8956-46c1-9ec1-879a4c793ab8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45694eaa-188f-4725-83b5-401740c99ae0",
                    "LayerId": "80fbf8aa-dc63-4c98-867a-005dae4b1e0c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 39,
    "layers": [
        {
            "id": "80fbf8aa-dc63-4c98-867a-005dae4b1e0c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "df989897-37ab-419a-ac19-6d14ca072e0a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 18,
    "yorig": 20
}