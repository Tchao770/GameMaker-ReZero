{
    "id": "f5743b1c-138f-425a-b0f8-356e5f710582",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 233,
    "bbox_left": 0,
    "bbox_right": 607,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8fa7ccfa-51a8-471a-975d-726db9a1d4ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5743b1c-138f-425a-b0f8-356e5f710582",
            "compositeImage": {
                "id": "8735d5a6-1b67-4943-a222-b642b3c8055b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fa7ccfa-51a8-471a-975d-726db9a1d4ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2172f3a3-5325-431c-8e08-ae2219d670b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fa7ccfa-51a8-471a-975d-726db9a1d4ad",
                    "LayerId": "f6353daa-568c-4b05-bbbc-64d2007979a7"
                }
            ]
        },
        {
            "id": "5330a1de-b7e8-401c-9c2d-7ee35e6ec639",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5743b1c-138f-425a-b0f8-356e5f710582",
            "compositeImage": {
                "id": "a0b5a08b-4313-41a5-8496-c173389efdbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5330a1de-b7e8-401c-9c2d-7ee35e6ec639",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "184d94ae-b08d-4858-9165-f929843b1531",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5330a1de-b7e8-401c-9c2d-7ee35e6ec639",
                    "LayerId": "f6353daa-568c-4b05-bbbc-64d2007979a7"
                }
            ]
        },
        {
            "id": "35d1cce6-4517-469a-b9fa-0485f7c4d292",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5743b1c-138f-425a-b0f8-356e5f710582",
            "compositeImage": {
                "id": "46dbb750-f4ab-420f-9c99-4a5a93f170b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35d1cce6-4517-469a-b9fa-0485f7c4d292",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0304c13f-80e4-42ac-b20b-d228ea442b6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35d1cce6-4517-469a-b9fa-0485f7c4d292",
                    "LayerId": "f6353daa-568c-4b05-bbbc-64d2007979a7"
                }
            ]
        },
        {
            "id": "be72afd7-306c-43fe-bd49-517a3be259bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5743b1c-138f-425a-b0f8-356e5f710582",
            "compositeImage": {
                "id": "6b64c9f0-c6a9-47b1-851a-dd1474de416f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be72afd7-306c-43fe-bd49-517a3be259bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "820187e8-e001-4200-9197-0323df5c0b6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be72afd7-306c-43fe-bd49-517a3be259bf",
                    "LayerId": "f6353daa-568c-4b05-bbbc-64d2007979a7"
                }
            ]
        },
        {
            "id": "089702dc-1008-4fa4-b445-e8f4cf92c822",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5743b1c-138f-425a-b0f8-356e5f710582",
            "compositeImage": {
                "id": "b36f23ad-de64-47e0-b96f-7887adf75c7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "089702dc-1008-4fa4-b445-e8f4cf92c822",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc76b86c-14eb-4690-8dc1-26666386e303",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "089702dc-1008-4fa4-b445-e8f4cf92c822",
                    "LayerId": "f6353daa-568c-4b05-bbbc-64d2007979a7"
                }
            ]
        },
        {
            "id": "d0fa4a43-b131-4660-bd38-b23bebf16fa2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5743b1c-138f-425a-b0f8-356e5f710582",
            "compositeImage": {
                "id": "c279263a-197d-43f0-9821-6f0b2e09fae8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0fa4a43-b131-4660-bd38-b23bebf16fa2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "078b7207-f8fd-4a70-8fe2-ac5e34d328b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0fa4a43-b131-4660-bd38-b23bebf16fa2",
                    "LayerId": "f6353daa-568c-4b05-bbbc-64d2007979a7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 234,
    "layers": [
        {
            "id": "f6353daa-568c-4b05-bbbc-64d2007979a7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f5743b1c-138f-425a-b0f8-356e5f710582",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 608,
    "xorig": 304,
    "yorig": 117
}