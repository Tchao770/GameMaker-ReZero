{
    "id": "b891d346-78a6-4385-9b87-f932c2618c66",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_jump_temp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 11,
    "bbox_right": 26,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1abd5f0e-6abb-4a1e-b3ba-ae6365ee3813",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b891d346-78a6-4385-9b87-f932c2618c66",
            "compositeImage": {
                "id": "e53f0dcd-fed7-4284-bd66-72ad0a99741b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1abd5f0e-6abb-4a1e-b3ba-ae6365ee3813",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6caaa1e5-60ef-4886-81c7-1380b4621c13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1abd5f0e-6abb-4a1e-b3ba-ae6365ee3813",
                    "LayerId": "480b1648-28f6-47fe-8515-ff8a0d1f4122"
                }
            ]
        },
        {
            "id": "31816211-0d6f-4540-8775-7d258aaa7d97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b891d346-78a6-4385-9b87-f932c2618c66",
            "compositeImage": {
                "id": "46af7213-bdcb-43ad-9ede-b64ca5a6772c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31816211-0d6f-4540-8775-7d258aaa7d97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e454bc3-1672-40df-81be-9cc76ffbbd1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31816211-0d6f-4540-8775-7d258aaa7d97",
                    "LayerId": "480b1648-28f6-47fe-8515-ff8a0d1f4122"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "480b1648-28f6-47fe-8515-ff8a0d1f4122",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b891d346-78a6-4385-9b87-f932c2618c66",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 38,
    "xorig": 18,
    "yorig": 20
}