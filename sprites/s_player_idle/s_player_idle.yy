{
    "id": "44ee1537-3a3b-4531-8805-2a4cf9390846",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 10,
    "bbox_right": 22,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "68833e31-573c-4532-b041-bd9092448be5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44ee1537-3a3b-4531-8805-2a4cf9390846",
            "compositeImage": {
                "id": "e43a44d2-904c-4323-ad2a-4d8621f89d31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68833e31-573c-4532-b041-bd9092448be5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8c015b7-9550-492d-b7a9-d9842fa419d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68833e31-573c-4532-b041-bd9092448be5",
                    "LayerId": "91b9e26c-5c64-40b2-90c5-22261729fd23"
                }
            ]
        },
        {
            "id": "4f1c8d47-a805-400c-b996-5546f3a47e39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44ee1537-3a3b-4531-8805-2a4cf9390846",
            "compositeImage": {
                "id": "1b1b8ca4-281d-4f37-9f75-e2d92d27b6a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f1c8d47-a805-400c-b996-5546f3a47e39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63fd5dec-e60d-4e70-b505-c34b521887b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f1c8d47-a805-400c-b996-5546f3a47e39",
                    "LayerId": "91b9e26c-5c64-40b2-90c5-22261729fd23"
                }
            ]
        },
        {
            "id": "c54281fb-2572-489f-b946-2a66619892a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44ee1537-3a3b-4531-8805-2a4cf9390846",
            "compositeImage": {
                "id": "4d0fea40-c1a1-45d8-9665-bb800b4babd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c54281fb-2572-489f-b946-2a66619892a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44a4ea0c-5da2-44ec-b08e-acdcf9d292d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c54281fb-2572-489f-b946-2a66619892a4",
                    "LayerId": "91b9e26c-5c64-40b2-90c5-22261729fd23"
                }
            ]
        },
        {
            "id": "bd73a1f9-6105-4b3d-b935-b6e11599bf6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44ee1537-3a3b-4531-8805-2a4cf9390846",
            "compositeImage": {
                "id": "30a5fdd6-145f-41a6-a404-f9a03ec9c05d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd73a1f9-6105-4b3d-b935-b6e11599bf6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99a96446-524c-4884-9982-520e56891c73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd73a1f9-6105-4b3d-b935-b6e11599bf6d",
                    "LayerId": "91b9e26c-5c64-40b2-90c5-22261729fd23"
                }
            ]
        },
        {
            "id": "f7e3cafc-fc62-465e-9fe0-d3fb7d598aa2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44ee1537-3a3b-4531-8805-2a4cf9390846",
            "compositeImage": {
                "id": "d136baf7-6042-4536-93e0-3ea2ce78430e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7e3cafc-fc62-465e-9fe0-d3fb7d598aa2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a73dd1c0-f13e-48e6-a9e2-d8696e10a846",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7e3cafc-fc62-465e-9fe0-d3fb7d598aa2",
                    "LayerId": "91b9e26c-5c64-40b2-90c5-22261729fd23"
                }
            ]
        },
        {
            "id": "fedcb648-b99e-4aa7-96da-9698b6366e12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44ee1537-3a3b-4531-8805-2a4cf9390846",
            "compositeImage": {
                "id": "0c25a2d3-7911-45d9-8f41-22f3d702cd3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fedcb648-b99e-4aa7-96da-9698b6366e12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e84cc3b-f19c-47e0-9c0d-60bd77f6e9a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fedcb648-b99e-4aa7-96da-9698b6366e12",
                    "LayerId": "91b9e26c-5c64-40b2-90c5-22261729fd23"
                }
            ]
        },
        {
            "id": "b6929053-37a9-45ee-aa81-58c470e70b62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44ee1537-3a3b-4531-8805-2a4cf9390846",
            "compositeImage": {
                "id": "3b826540-d757-4fca-8d58-5c74207fa1b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6929053-37a9-45ee-aa81-58c470e70b62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6ca6df1-1d62-4c7a-88d8-de2d41dc87f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6929053-37a9-45ee-aa81-58c470e70b62",
                    "LayerId": "91b9e26c-5c64-40b2-90c5-22261729fd23"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 41,
    "layers": [
        {
            "id": "91b9e26c-5c64-40b2-90c5-22261729fd23",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "44ee1537-3a3b-4531-8805-2a4cf9390846",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 36,
    "xorig": 18,
    "yorig": 20
}