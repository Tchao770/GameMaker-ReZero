{
    "id": "3e935835-0996-4978-a277-fd24f4bdec5a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_attack2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 0,
    "bbox_right": 64,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ea07bf0d-1dbc-4c96-bb12-13b37b261b10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e935835-0996-4978-a277-fd24f4bdec5a",
            "compositeImage": {
                "id": "d08b520f-1e23-47af-885e-5258e0ce78b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea07bf0d-1dbc-4c96-bb12-13b37b261b10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9aa9dabe-a979-4593-9cfa-1aee3210fd49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea07bf0d-1dbc-4c96-bb12-13b37b261b10",
                    "LayerId": "284f4d9e-0bf2-4a05-a0fa-ad33b47e5a13"
                }
            ]
        },
        {
            "id": "c8bb5c4a-b834-4620-91d1-26736ceef564",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e935835-0996-4978-a277-fd24f4bdec5a",
            "compositeImage": {
                "id": "92b7df1f-1dc2-47f2-b71f-6bd9b3abffbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8bb5c4a-b834-4620-91d1-26736ceef564",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9fa8a67-897f-4e4c-a90b-0aca68938960",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8bb5c4a-b834-4620-91d1-26736ceef564",
                    "LayerId": "284f4d9e-0bf2-4a05-a0fa-ad33b47e5a13"
                }
            ]
        },
        {
            "id": "888946b4-e067-4d1d-bb64-a31903718602",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e935835-0996-4978-a277-fd24f4bdec5a",
            "compositeImage": {
                "id": "f87f7c56-0b08-4f8b-9425-e8f83fc7ba56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "888946b4-e067-4d1d-bb64-a31903718602",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b53fc7ca-0de0-42d9-ba8c-e20eb7bc528c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "888946b4-e067-4d1d-bb64-a31903718602",
                    "LayerId": "284f4d9e-0bf2-4a05-a0fa-ad33b47e5a13"
                }
            ]
        },
        {
            "id": "8ac4b122-28ef-4474-93ad-4601c6338be0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e935835-0996-4978-a277-fd24f4bdec5a",
            "compositeImage": {
                "id": "ec6a31d9-ef9c-4900-a5fd-b49ac7199330",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ac4b122-28ef-4474-93ad-4601c6338be0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ec9513a-1ae1-432a-a481-e4c480357a47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ac4b122-28ef-4474-93ad-4601c6338be0",
                    "LayerId": "284f4d9e-0bf2-4a05-a0fa-ad33b47e5a13"
                }
            ]
        },
        {
            "id": "c0fd7473-a53f-4c1b-b653-31c5739b5362",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e935835-0996-4978-a277-fd24f4bdec5a",
            "compositeImage": {
                "id": "f3cbc355-d0d3-49a6-9ba8-65dc51617bc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0fd7473-a53f-4c1b-b653-31c5739b5362",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01ad7567-e46e-41d1-9a24-2086c4f78713",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0fd7473-a53f-4c1b-b653-31c5739b5362",
                    "LayerId": "284f4d9e-0bf2-4a05-a0fa-ad33b47e5a13"
                }
            ]
        },
        {
            "id": "e10ad055-be00-49bd-901e-eeb6bacfcfc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e935835-0996-4978-a277-fd24f4bdec5a",
            "compositeImage": {
                "id": "8fe70862-3472-429b-8880-1680c595ac53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e10ad055-be00-49bd-901e-eeb6bacfcfc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84b67405-7521-4a42-af16-34ac25c732d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e10ad055-be00-49bd-901e-eeb6bacfcfc0",
                    "LayerId": "284f4d9e-0bf2-4a05-a0fa-ad33b47e5a13"
                }
            ]
        },
        {
            "id": "52d0fe3a-4d0d-4f67-89b7-be6849455375",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e935835-0996-4978-a277-fd24f4bdec5a",
            "compositeImage": {
                "id": "c7aa57a0-ad69-4d85-9555-52ea03d5b47f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52d0fe3a-4d0d-4f67-89b7-be6849455375",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "623fea46-2dc7-4e97-900a-5d312fe1737f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52d0fe3a-4d0d-4f67-89b7-be6849455375",
                    "LayerId": "284f4d9e-0bf2-4a05-a0fa-ad33b47e5a13"
                }
            ]
        },
        {
            "id": "99e37aa3-33fd-4d98-b149-6854a7e2d732",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e935835-0996-4978-a277-fd24f4bdec5a",
            "compositeImage": {
                "id": "8e4addf6-69e8-4de7-b23d-9b83ed5e53c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99e37aa3-33fd-4d98-b149-6854a7e2d732",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85222095-1271-4919-9600-89654a0c436c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99e37aa3-33fd-4d98-b149-6854a7e2d732",
                    "LayerId": "284f4d9e-0bf2-4a05-a0fa-ad33b47e5a13"
                }
            ]
        },
        {
            "id": "3c8b70e0-17d3-4830-9bb6-9a684005ba88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e935835-0996-4978-a277-fd24f4bdec5a",
            "compositeImage": {
                "id": "7b4ac4b4-0bb1-4161-a1f7-d9f9bbedc162",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c8b70e0-17d3-4830-9bb6-9a684005ba88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08a2bf9b-ac3c-4ecf-a882-7d5fc7e24113",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c8b70e0-17d3-4830-9bb6-9a684005ba88",
                    "LayerId": "284f4d9e-0bf2-4a05-a0fa-ad33b47e5a13"
                }
            ]
        },
        {
            "id": "a9df2506-4801-4c01-ad34-c8aed8d2143c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e935835-0996-4978-a277-fd24f4bdec5a",
            "compositeImage": {
                "id": "459c1110-3238-457f-9698-6cc02d425c74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9df2506-4801-4c01-ad34-c8aed8d2143c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "834b5632-baaa-43d7-b5e1-494b2a4efd67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9df2506-4801-4c01-ad34-c8aed8d2143c",
                    "LayerId": "284f4d9e-0bf2-4a05-a0fa-ad33b47e5a13"
                }
            ]
        },
        {
            "id": "897a9b3e-2b55-44f5-89cc-131d7342b3aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e935835-0996-4978-a277-fd24f4bdec5a",
            "compositeImage": {
                "id": "4501875b-0597-4101-898c-a279fda5a084",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "897a9b3e-2b55-44f5-89cc-131d7342b3aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "508c1258-09e1-47f2-a1d3-215618eb9f5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "897a9b3e-2b55-44f5-89cc-131d7342b3aa",
                    "LayerId": "284f4d9e-0bf2-4a05-a0fa-ad33b47e5a13"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 49,
    "layers": [
        {
            "id": "284f4d9e-0bf2-4a05-a0fa-ad33b47e5a13",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e935835-0996-4978-a277-fd24f4bdec5a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 65,
    "xorig": 32,
    "yorig": 24
}