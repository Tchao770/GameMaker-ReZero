{
    "id": "5f332395-a272-43d3-ba4a-bf855368277e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 294,
    "bbox_left": 40,
    "bbox_right": 321,
    "bbox_top": 33,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "be543c59-784e-409b-adff-e131601605a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f332395-a272-43d3-ba4a-bf855368277e",
            "compositeImage": {
                "id": "f85a31ee-2799-47c8-92c7-80ff8c73569c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be543c59-784e-409b-adff-e131601605a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8f7ef0d-a3db-430f-8a3b-e11f0cc17a67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be543c59-784e-409b-adff-e131601605a4",
                    "LayerId": "8fb75de9-f48a-4c76-b372-26cca802ceb3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 340,
    "layers": [
        {
            "id": "8fb75de9-f48a-4c76-b372-26cca802ceb3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5f332395-a272-43d3-ba4a-bf855368277e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 357,
    "xorig": 178,
    "yorig": 170
}