{
    "id": "d9675ffd-a025-4741-adb0-f74d29fd4064",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_dash",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 36,
    "bbox_left": 0,
    "bbox_right": 44,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9514923e-d9f1-46dd-afb2-372f1c06a4f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9675ffd-a025-4741-adb0-f74d29fd4064",
            "compositeImage": {
                "id": "3d235d68-3ba1-4583-9968-f0a95102fef3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9514923e-d9f1-46dd-afb2-372f1c06a4f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fd7df4f-920c-47c7-943c-eb461c6c37b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9514923e-d9f1-46dd-afb2-372f1c06a4f0",
                    "LayerId": "e78662e7-85c0-4027-9630-07992c161e7a"
                },
                {
                    "id": "e543f831-54c6-400a-afd8-3387f6888cb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9514923e-d9f1-46dd-afb2-372f1c06a4f0",
                    "LayerId": "acfcdc95-794c-418a-8684-0b3c062b9295"
                }
            ]
        },
        {
            "id": "d643a7cf-6086-4e23-af6c-715cc9753a93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9675ffd-a025-4741-adb0-f74d29fd4064",
            "compositeImage": {
                "id": "b4fad016-96b5-471c-af08-9d8f4eff36f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d643a7cf-6086-4e23-af6c-715cc9753a93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d12b153-6174-45c7-858b-d471cccbf16e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d643a7cf-6086-4e23-af6c-715cc9753a93",
                    "LayerId": "e78662e7-85c0-4027-9630-07992c161e7a"
                },
                {
                    "id": "4eb10369-4253-481a-a5ee-22a3e828819d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d643a7cf-6086-4e23-af6c-715cc9753a93",
                    "LayerId": "acfcdc95-794c-418a-8684-0b3c062b9295"
                }
            ]
        },
        {
            "id": "ac3e02de-7e7d-42a6-a2e7-9d3ffe628773",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9675ffd-a025-4741-adb0-f74d29fd4064",
            "compositeImage": {
                "id": "328c289e-6258-4c18-bec0-1d9d7bb562b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac3e02de-7e7d-42a6-a2e7-9d3ffe628773",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21510dc9-d006-4887-b341-57962c81d7cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac3e02de-7e7d-42a6-a2e7-9d3ffe628773",
                    "LayerId": "e78662e7-85c0-4027-9630-07992c161e7a"
                },
                {
                    "id": "0493ceba-311c-4f43-97fc-901e93822a29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac3e02de-7e7d-42a6-a2e7-9d3ffe628773",
                    "LayerId": "acfcdc95-794c-418a-8684-0b3c062b9295"
                }
            ]
        },
        {
            "id": "f6bb36b0-f456-44c0-838a-f86014e73685",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9675ffd-a025-4741-adb0-f74d29fd4064",
            "compositeImage": {
                "id": "5b38e423-3970-49e4-b0a8-7eeb67121a56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6bb36b0-f456-44c0-838a-f86014e73685",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84de1319-ca6a-4533-9cb8-4630862aceac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6bb36b0-f456-44c0-838a-f86014e73685",
                    "LayerId": "e78662e7-85c0-4027-9630-07992c161e7a"
                },
                {
                    "id": "448bd2a2-3fcd-4723-9593-1e3f812ede42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6bb36b0-f456-44c0-838a-f86014e73685",
                    "LayerId": "acfcdc95-794c-418a-8684-0b3c062b9295"
                }
            ]
        },
        {
            "id": "70cc2cbc-ff19-4d03-baee-9da83fe0e21e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9675ffd-a025-4741-adb0-f74d29fd4064",
            "compositeImage": {
                "id": "a523a39a-fda8-4f71-949f-71077b121005",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70cc2cbc-ff19-4d03-baee-9da83fe0e21e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c50a5078-8ce3-4de7-b15c-7a9a8a826163",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70cc2cbc-ff19-4d03-baee-9da83fe0e21e",
                    "LayerId": "e78662e7-85c0-4027-9630-07992c161e7a"
                },
                {
                    "id": "35eb8711-a2aa-4811-a4a5-07c191b74d89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70cc2cbc-ff19-4d03-baee-9da83fe0e21e",
                    "LayerId": "acfcdc95-794c-418a-8684-0b3c062b9295"
                }
            ]
        },
        {
            "id": "56cd7a4c-2dd8-4c55-9a52-d0f676a3e773",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9675ffd-a025-4741-adb0-f74d29fd4064",
            "compositeImage": {
                "id": "5c0e4159-b171-4887-b026-814e294b582d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56cd7a4c-2dd8-4c55-9a52-d0f676a3e773",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e986e54-dd5c-49cb-bc92-736c278e27e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56cd7a4c-2dd8-4c55-9a52-d0f676a3e773",
                    "LayerId": "e78662e7-85c0-4027-9630-07992c161e7a"
                },
                {
                    "id": "362e7d2e-9a5d-4a82-9be5-10925d2aeadf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56cd7a4c-2dd8-4c55-9a52-d0f676a3e773",
                    "LayerId": "acfcdc95-794c-418a-8684-0b3c062b9295"
                }
            ]
        },
        {
            "id": "fd12e346-5a54-4212-aa02-cec1a7f5c371",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9675ffd-a025-4741-adb0-f74d29fd4064",
            "compositeImage": {
                "id": "f0e4e24b-bcd1-42f9-b519-d499620ed3d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd12e346-5a54-4212-aa02-cec1a7f5c371",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6058d9a9-dd21-4bee-8ca9-59d4fe1dc349",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd12e346-5a54-4212-aa02-cec1a7f5c371",
                    "LayerId": "e78662e7-85c0-4027-9630-07992c161e7a"
                },
                {
                    "id": "b86b9fac-4bd3-439a-afeb-02e2bcd67a5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd12e346-5a54-4212-aa02-cec1a7f5c371",
                    "LayerId": "acfcdc95-794c-418a-8684-0b3c062b9295"
                }
            ]
        },
        {
            "id": "a67888a1-9f88-400e-b18a-d82a18b2aafa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9675ffd-a025-4741-adb0-f74d29fd4064",
            "compositeImage": {
                "id": "e1aef46a-1a05-4aae-8dde-772c3e6cc06e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a67888a1-9f88-400e-b18a-d82a18b2aafa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "533b8e52-cb1e-4c1c-b0e4-929d317b59f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a67888a1-9f88-400e-b18a-d82a18b2aafa",
                    "LayerId": "e78662e7-85c0-4027-9630-07992c161e7a"
                },
                {
                    "id": "e3b8859a-4a44-445c-bb5b-3f76e75d33ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a67888a1-9f88-400e-b18a-d82a18b2aafa",
                    "LayerId": "acfcdc95-794c-418a-8684-0b3c062b9295"
                }
            ]
        },
        {
            "id": "1b8f240a-d852-4e77-839f-203b88a61b23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9675ffd-a025-4741-adb0-f74d29fd4064",
            "compositeImage": {
                "id": "8f934cd7-4b14-478e-9769-998e02234ea0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b8f240a-d852-4e77-839f-203b88a61b23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b98af8d7-ed73-428b-8c4d-54b1b1a88772",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b8f240a-d852-4e77-839f-203b88a61b23",
                    "LayerId": "e78662e7-85c0-4027-9630-07992c161e7a"
                },
                {
                    "id": "56eee27d-c0db-4794-b6a9-e21af1369209",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b8f240a-d852-4e77-839f-203b88a61b23",
                    "LayerId": "acfcdc95-794c-418a-8684-0b3c062b9295"
                }
            ]
        },
        {
            "id": "7972822e-5832-4d1d-b4be-261f67d6a8b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9675ffd-a025-4741-adb0-f74d29fd4064",
            "compositeImage": {
                "id": "0ce17d0b-6a3c-4b57-b7d3-1d9e5483d2f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7972822e-5832-4d1d-b4be-261f67d6a8b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "daf92b87-8056-4ebf-8f44-064083fd8c9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7972822e-5832-4d1d-b4be-261f67d6a8b6",
                    "LayerId": "e78662e7-85c0-4027-9630-07992c161e7a"
                },
                {
                    "id": "fa4c00ea-59a8-45b6-a990-b3a602063731",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7972822e-5832-4d1d-b4be-261f67d6a8b6",
                    "LayerId": "acfcdc95-794c-418a-8684-0b3c062b9295"
                }
            ]
        },
        {
            "id": "0ef586d4-16f3-40b4-9726-a792cb1a4915",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9675ffd-a025-4741-adb0-f74d29fd4064",
            "compositeImage": {
                "id": "4123bc45-52db-4d8b-99e6-5da80818a80f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ef586d4-16f3-40b4-9726-a792cb1a4915",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d963a4f-7775-4814-ac1a-5cc8acc713ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ef586d4-16f3-40b4-9726-a792cb1a4915",
                    "LayerId": "e78662e7-85c0-4027-9630-07992c161e7a"
                },
                {
                    "id": "a0a893ef-77bc-45f1-9129-6c11cf4d7235",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ef586d4-16f3-40b4-9726-a792cb1a4915",
                    "LayerId": "acfcdc95-794c-418a-8684-0b3c062b9295"
                }
            ]
        },
        {
            "id": "4a9ac313-d863-4203-9dbb-1fbabc8a74c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9675ffd-a025-4741-adb0-f74d29fd4064",
            "compositeImage": {
                "id": "b0ab9874-34fa-43e7-b7f3-06d800525ec9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a9ac313-d863-4203-9dbb-1fbabc8a74c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "384e5bd4-25d2-4b04-ad2c-72f296dc163d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a9ac313-d863-4203-9dbb-1fbabc8a74c2",
                    "LayerId": "e78662e7-85c0-4027-9630-07992c161e7a"
                },
                {
                    "id": "44c776fb-c73d-480f-ac96-74c33e51eb70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a9ac313-d863-4203-9dbb-1fbabc8a74c2",
                    "LayerId": "acfcdc95-794c-418a-8684-0b3c062b9295"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 37,
    "layers": [
        {
            "id": "acfcdc95-794c-418a-8684-0b3c062b9295",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d9675ffd-a025-4741-adb0-f74d29fd4064",
            "blendMode": 0,
            "isLocked": false,
            "name": "Dash_Effect",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "e78662e7-85c0-4027-9630-07992c161e7a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d9675ffd-a025-4741-adb0-f74d29fd4064",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 45,
    "xorig": 22,
    "yorig": 18
}