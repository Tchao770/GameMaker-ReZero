{
    "id": "83e7420e-ec4c-4e42-b101-665e33b68bd8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_attack1_hitbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 5,
    "bbox_right": 60,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "063ce935-bbd2-4dfc-ac1f-754fbab1def0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83e7420e-ec4c-4e42-b101-665e33b68bd8",
            "compositeImage": {
                "id": "b5949c17-46ec-4280-b5b0-4468be8f3763",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "063ce935-bbd2-4dfc-ac1f-754fbab1def0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa375e39-2ed5-4d0b-9ea8-a3572be6d355",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "063ce935-bbd2-4dfc-ac1f-754fbab1def0",
                    "LayerId": "0ae334db-bc0a-4a42-855a-5628d1e7ad80"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 42,
    "layers": [
        {
            "id": "0ae334db-bc0a-4a42-855a-5628d1e7ad80",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "83e7420e-ec4c-4e42-b101-665e33b68bd8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 61,
    "xorig": 13,
    "yorig": 17
}