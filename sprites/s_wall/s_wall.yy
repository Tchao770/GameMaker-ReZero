{
    "id": "28e26814-e225-4466-975b-850ce22ec3cf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 16,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "01e37df2-2544-4c9d-be8d-51a573d93811",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28e26814-e225-4466-975b-850ce22ec3cf",
            "compositeImage": {
                "id": "0cbd4f50-1a92-426e-971d-198910b27196",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01e37df2-2544-4c9d-be8d-51a573d93811",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fdf92a3-b928-4475-859c-5a52bdc9bdb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01e37df2-2544-4c9d-be8d-51a573d93811",
                    "LayerId": "4768e096-c73b-4363-86f7-c6a5a6e11981"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "4768e096-c73b-4363-86f7-c6a5a6e11981",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "28e26814-e225-4466-975b-850ce22ec3cf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}