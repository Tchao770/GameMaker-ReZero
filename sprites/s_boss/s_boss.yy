{
    "id": "97fe7fe9-ce48-4c27-867d-4cf07c052d76",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_boss",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 257,
    "bbox_left": 0,
    "bbox_right": 191,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aa951e16-3aa0-4942-b085-2f1e26dcec1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97fe7fe9-ce48-4c27-867d-4cf07c052d76",
            "compositeImage": {
                "id": "7e681171-74ff-435e-9f3f-16b0f52ce203",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa951e16-3aa0-4942-b085-2f1e26dcec1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c58e048-b625-45ca-8c6e-4f4b0367a8f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa951e16-3aa0-4942-b085-2f1e26dcec1e",
                    "LayerId": "3faafa82-64b4-47b3-8752-217766360f38"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 258,
    "layers": [
        {
            "id": "3faafa82-64b4-47b3-8752-217766360f38",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "97fe7fe9-ce48-4c27-867d-4cf07c052d76",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 149,
    "yorig": 61
}