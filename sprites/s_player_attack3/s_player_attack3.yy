{
    "id": "9ed04ceb-db05-432e-983c-680307a4d34e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_attack3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 0,
    "bbox_right": 79,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "418ad80e-761d-4fc0-85ff-5df46b31d6e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ed04ceb-db05-432e-983c-680307a4d34e",
            "compositeImage": {
                "id": "58a9c243-b796-4615-9e77-424e6b6f96d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "418ad80e-761d-4fc0-85ff-5df46b31d6e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69d6205c-a728-4d2d-a815-8c721b3d291a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "418ad80e-761d-4fc0-85ff-5df46b31d6e7",
                    "LayerId": "7073487e-d4af-4db6-8a2e-5486e50639e1"
                }
            ]
        },
        {
            "id": "35d476bd-0af8-4577-a344-341f14fa65de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ed04ceb-db05-432e-983c-680307a4d34e",
            "compositeImage": {
                "id": "42d5f80c-8b36-4d8d-aa0f-e79d4ec400cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35d476bd-0af8-4577-a344-341f14fa65de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e87fd11-9733-4744-893d-bb3fd3823382",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35d476bd-0af8-4577-a344-341f14fa65de",
                    "LayerId": "7073487e-d4af-4db6-8a2e-5486e50639e1"
                }
            ]
        },
        {
            "id": "2db86105-387c-4040-a8ba-75f4935a926a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ed04ceb-db05-432e-983c-680307a4d34e",
            "compositeImage": {
                "id": "2ce33325-8e12-444b-87d9-e2de7ebf944d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2db86105-387c-4040-a8ba-75f4935a926a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e3b948c-b39f-4059-a224-f75095a36fe2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2db86105-387c-4040-a8ba-75f4935a926a",
                    "LayerId": "7073487e-d4af-4db6-8a2e-5486e50639e1"
                }
            ]
        },
        {
            "id": "ee973b26-5920-498a-bccb-c57625866950",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ed04ceb-db05-432e-983c-680307a4d34e",
            "compositeImage": {
                "id": "b28938f7-dba4-415f-898b-fa0a4e156c2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee973b26-5920-498a-bccb-c57625866950",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "042b0276-ba08-429a-af1d-215c95104ecf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee973b26-5920-498a-bccb-c57625866950",
                    "LayerId": "7073487e-d4af-4db6-8a2e-5486e50639e1"
                }
            ]
        },
        {
            "id": "0d186925-f9a4-4016-9db2-b85cd6187ed6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ed04ceb-db05-432e-983c-680307a4d34e",
            "compositeImage": {
                "id": "acb4c5e2-fdf4-4219-b471-d6e68857f3cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d186925-f9a4-4016-9db2-b85cd6187ed6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ec03a3d-b299-4d45-b70e-08443b513e28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d186925-f9a4-4016-9db2-b85cd6187ed6",
                    "LayerId": "7073487e-d4af-4db6-8a2e-5486e50639e1"
                }
            ]
        },
        {
            "id": "30b7972b-7809-4ca4-986e-064d7f429694",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ed04ceb-db05-432e-983c-680307a4d34e",
            "compositeImage": {
                "id": "c540a88a-4527-43bb-985c-eefb37d26386",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30b7972b-7809-4ca4-986e-064d7f429694",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f127e5f-79f9-4821-b674-9ace1b524eff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30b7972b-7809-4ca4-986e-064d7f429694",
                    "LayerId": "7073487e-d4af-4db6-8a2e-5486e50639e1"
                }
            ]
        },
        {
            "id": "68aa18a5-77a7-4d8b-8543-df9d84623672",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ed04ceb-db05-432e-983c-680307a4d34e",
            "compositeImage": {
                "id": "50520cc4-ba19-4898-8bd2-5baddf89e4ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68aa18a5-77a7-4d8b-8543-df9d84623672",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28c05a74-1d26-4453-bffb-d5a2900d2293",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68aa18a5-77a7-4d8b-8543-df9d84623672",
                    "LayerId": "7073487e-d4af-4db6-8a2e-5486e50639e1"
                }
            ]
        },
        {
            "id": "d6af36c1-3346-4816-8569-b80f3af03346",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ed04ceb-db05-432e-983c-680307a4d34e",
            "compositeImage": {
                "id": "76e77dfe-9516-409d-a027-66f6ca9b8401",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6af36c1-3346-4816-8569-b80f3af03346",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "276b0d9d-77ae-4451-9a2a-ff165e0492a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6af36c1-3346-4816-8569-b80f3af03346",
                    "LayerId": "7073487e-d4af-4db6-8a2e-5486e50639e1"
                }
            ]
        },
        {
            "id": "25d36636-373b-488e-9aae-20f8b3c64e13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ed04ceb-db05-432e-983c-680307a4d34e",
            "compositeImage": {
                "id": "80def490-1244-482d-885b-36e3adbbb8e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25d36636-373b-488e-9aae-20f8b3c64e13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcb19c1e-eecd-429c-9a98-57de6f433977",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25d36636-373b-488e-9aae-20f8b3c64e13",
                    "LayerId": "7073487e-d4af-4db6-8a2e-5486e50639e1"
                }
            ]
        },
        {
            "id": "d7be547e-85db-42bd-8a07-9b756a0e97d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ed04ceb-db05-432e-983c-680307a4d34e",
            "compositeImage": {
                "id": "c35d8616-00f3-4217-b208-6eedcbbc515e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7be547e-85db-42bd-8a07-9b756a0e97d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "307ac985-0ac3-402c-ae60-79bf8c145288",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7be547e-85db-42bd-8a07-9b756a0e97d4",
                    "LayerId": "7073487e-d4af-4db6-8a2e-5486e50639e1"
                }
            ]
        },
        {
            "id": "ed4d3861-8e0e-4a25-a234-723a8731ddff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ed04ceb-db05-432e-983c-680307a4d34e",
            "compositeImage": {
                "id": "83454056-9136-4c8e-aa5e-33ef00ad9a11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed4d3861-8e0e-4a25-a234-723a8731ddff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1cd5c88-074c-4854-98a5-7ed8a756f5fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed4d3861-8e0e-4a25-a234-723a8731ddff",
                    "LayerId": "7073487e-d4af-4db6-8a2e-5486e50639e1"
                }
            ]
        },
        {
            "id": "e5568e7e-e8e0-4d47-82f1-ca363a6b059a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ed04ceb-db05-432e-983c-680307a4d34e",
            "compositeImage": {
                "id": "7422fee6-bb6f-4f72-bf97-b51cba533ae0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5568e7e-e8e0-4d47-82f1-ca363a6b059a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47f2f69e-299f-4b81-9f8b-613e881a3b30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5568e7e-e8e0-4d47-82f1-ca363a6b059a",
                    "LayerId": "7073487e-d4af-4db6-8a2e-5486e50639e1"
                }
            ]
        },
        {
            "id": "535286cb-2ecc-4a44-ac36-95c3a6c6e63e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ed04ceb-db05-432e-983c-680307a4d34e",
            "compositeImage": {
                "id": "70a2a731-e73f-406c-bc06-662688a5dfa7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "535286cb-2ecc-4a44-ac36-95c3a6c6e63e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba0260a7-c390-49e4-b4b9-e2aac6ca0b1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "535286cb-2ecc-4a44-ac36-95c3a6c6e63e",
                    "LayerId": "7073487e-d4af-4db6-8a2e-5486e50639e1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 62,
    "layers": [
        {
            "id": "7073487e-d4af-4db6-8a2e-5486e50639e1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9ed04ceb-db05-432e-983c-680307a4d34e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 31
}