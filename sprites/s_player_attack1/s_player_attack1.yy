{
    "id": "13f3b30b-ec5e-4c9d-a59d-24bd6dd297e9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_attack1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 11,
    "bbox_right": 26,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "79a823dd-799a-4df7-bc8f-c6fc920f6bc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f3b30b-ec5e-4c9d-a59d-24bd6dd297e9",
            "compositeImage": {
                "id": "c0172334-c072-48a7-ad8e-1910025f8b42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79a823dd-799a-4df7-bc8f-c6fc920f6bc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fda83295-612f-4754-ae03-2b6b132343d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79a823dd-799a-4df7-bc8f-c6fc920f6bc5",
                    "LayerId": "aa72c398-f822-4c75-a336-89c59a2b5892"
                }
            ]
        },
        {
            "id": "7db77202-d38c-407b-8e56-3eb045b8dbf1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f3b30b-ec5e-4c9d-a59d-24bd6dd297e9",
            "compositeImage": {
                "id": "4e8a3c02-92d0-464c-8c6f-ddaef83de890",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7db77202-d38c-407b-8e56-3eb045b8dbf1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "021000cb-1409-431d-8881-461ff444cc97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7db77202-d38c-407b-8e56-3eb045b8dbf1",
                    "LayerId": "aa72c398-f822-4c75-a336-89c59a2b5892"
                }
            ]
        },
        {
            "id": "e6ec52fd-1eea-4065-8651-cae100259991",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f3b30b-ec5e-4c9d-a59d-24bd6dd297e9",
            "compositeImage": {
                "id": "b68e69c8-9059-487c-b93b-ad6e25a72dfd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6ec52fd-1eea-4065-8651-cae100259991",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9ef23a5-5ba9-400c-9462-796d253f0790",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6ec52fd-1eea-4065-8651-cae100259991",
                    "LayerId": "aa72c398-f822-4c75-a336-89c59a2b5892"
                }
            ]
        },
        {
            "id": "2eb2348d-5d52-48fe-80c4-a28cfa1234b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f3b30b-ec5e-4c9d-a59d-24bd6dd297e9",
            "compositeImage": {
                "id": "5ed80466-c038-4d21-84ce-fc8fa91d8736",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2eb2348d-5d52-48fe-80c4-a28cfa1234b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39bda39f-b888-460b-a5df-7fbe752e354e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2eb2348d-5d52-48fe-80c4-a28cfa1234b4",
                    "LayerId": "aa72c398-f822-4c75-a336-89c59a2b5892"
                }
            ]
        },
        {
            "id": "8d382c1f-a1c0-40db-9807-5e40043e0406",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f3b30b-ec5e-4c9d-a59d-24bd6dd297e9",
            "compositeImage": {
                "id": "0d26534c-fb29-4a76-83dc-55f2fed8751f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d382c1f-a1c0-40db-9807-5e40043e0406",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77f6994f-5ce5-4c06-a735-56c834cfce39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d382c1f-a1c0-40db-9807-5e40043e0406",
                    "LayerId": "aa72c398-f822-4c75-a336-89c59a2b5892"
                }
            ]
        },
        {
            "id": "6556e509-8003-44ff-9f8a-54e5dfd98c28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f3b30b-ec5e-4c9d-a59d-24bd6dd297e9",
            "compositeImage": {
                "id": "984d9878-7ecd-4871-92a6-3a2e105ff65b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6556e509-8003-44ff-9f8a-54e5dfd98c28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d0e8399-8479-4fc9-92f8-08cb34a8090a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6556e509-8003-44ff-9f8a-54e5dfd98c28",
                    "LayerId": "aa72c398-f822-4c75-a336-89c59a2b5892"
                }
            ]
        },
        {
            "id": "573dc9d7-5f6c-4f49-a6d2-8cfc7e46b3fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f3b30b-ec5e-4c9d-a59d-24bd6dd297e9",
            "compositeImage": {
                "id": "552f8ad1-a7f9-465f-933e-2d24138b0bfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "573dc9d7-5f6c-4f49-a6d2-8cfc7e46b3fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50b4cb4b-9f12-4c80-880a-b9b8557d082b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "573dc9d7-5f6c-4f49-a6d2-8cfc7e46b3fb",
                    "LayerId": "aa72c398-f822-4c75-a336-89c59a2b5892"
                }
            ]
        },
        {
            "id": "7b93d5db-54d5-472c-9e22-0a317e51cc5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f3b30b-ec5e-4c9d-a59d-24bd6dd297e9",
            "compositeImage": {
                "id": "c51870fb-364e-4ba4-8bf5-e05d41fe37ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b93d5db-54d5-472c-9e22-0a317e51cc5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99891a35-1fef-4591-b5f8-e01b6d28a293",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b93d5db-54d5-472c-9e22-0a317e51cc5e",
                    "LayerId": "aa72c398-f822-4c75-a336-89c59a2b5892"
                }
            ]
        },
        {
            "id": "ae80166f-130e-4583-a041-9ae861d1f89f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f3b30b-ec5e-4c9d-a59d-24bd6dd297e9",
            "compositeImage": {
                "id": "24713b14-44eb-4ef3-b969-21dba2c0a2ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae80166f-130e-4583-a041-9ae861d1f89f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b50fc071-b8c5-4d51-bfcb-ae1f3c468d1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae80166f-130e-4583-a041-9ae861d1f89f",
                    "LayerId": "aa72c398-f822-4c75-a336-89c59a2b5892"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 42,
    "layers": [
        {
            "id": "aa72c398-f822-4c75-a336-89c59a2b5892",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "13f3b30b-ec5e-4c9d-a59d-24bd6dd297e9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 75,
    "xorig": 28,
    "yorig": 20
}