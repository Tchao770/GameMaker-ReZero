{
    "id": "8ee17ebe-a3b4-4b5e-8fb0-3442a3b91d59",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_projectile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 217,
    "bbox_left": 311,
    "bbox_right": 653,
    "bbox_top": 142,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4a8159f3-8b6c-4cd3-9233-821b5374ccd2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ee17ebe-a3b4-4b5e-8fb0-3442a3b91d59",
            "compositeImage": {
                "id": "4fb242b9-6e11-48ce-be64-27826891065d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a8159f3-8b6c-4cd3-9233-821b5374ccd2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a26d97b-121d-4a48-bcb9-f5b1ce0ba910",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a8159f3-8b6c-4cd3-9233-821b5374ccd2",
                    "LayerId": "206e8ead-9a15-4222-9499-d28e8cb53267"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 368,
    "layers": [
        {
            "id": "206e8ead-9a15-4222-9499-d28e8cb53267",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8ee17ebe-a3b4-4b5e-8fb0-3442a3b91d59",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 900,
    "xorig": 747,
    "yorig": 164
}