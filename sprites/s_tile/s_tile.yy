{
    "id": "98c39d3a-9da3-4f92-96f6-7b3e0ead6a01",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_tile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 383,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2e3939b5-6ed7-4488-aa54-6a4931d82f62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98c39d3a-9da3-4f92-96f6-7b3e0ead6a01",
            "compositeImage": {
                "id": "a7f6e895-1106-4e4d-9b73-fb44ceb8adc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e3939b5-6ed7-4488-aa54-6a4931d82f62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6f41aee-d975-4704-99ef-a7230412c0de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e3939b5-6ed7-4488-aa54-6a4931d82f62",
                    "LayerId": "b7f5ef15-2322-46ac-916c-0c04e2721f30"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "b7f5ef15-2322-46ac-916c-0c04e2721f30",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "98c39d3a-9da3-4f92-96f6-7b3e0ead6a01",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 384,
    "xorig": 0,
    "yorig": 0
}